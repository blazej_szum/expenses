var gulp = require('gulp');
var args = require('yargs').argv;
var del = require('del');
var browserSync = require('browser-sync').create();
var $ = require('gulp-load-plugins')({lazy: true});
var config = require('./gulp.config')({lazy: true});
var port = process.env.PORT || config.defaultPort;

// Static server
gulp.task('serve', function () {

    var url = require('url');
    var proxy = require('proxy-middleware');
    var proxyOptions = url.parse('http://localhost:8080/expenses');
    proxyOptions.route = '/expenses';

    browserSync.init({
        server: {
            baseDir: "./src/",
            middleware: [function (req, res, next) {
                req.headers['X-Forwarded-Host'] = req.headers.host;
                next();
            },
            proxy(proxyOptions)]
        }
    });
});

//  browserSync({
//         proxy: {
//         target: 'http://localhost:8082',
//         reqHeaders: function (config) {
//             return {
//                 // prevent Host header overriding
//                 //"host":            config.urlObj.host,
//                 "accept-encoding": "identity", // disable any compression
//                 "agent":           false
//             };
//         },
//         middleware: function (req, res, next) {
//             res.setHeader("X-Forwarded-For", req.ip);
//             res.setHeader("X-Forwarded-Host", req.headers.host);
//             next();
//         }
//     }
// });


// gulp.task('serve', function(){
//     var isDev = true;
//
//     var nodeOptions = {
//        script: config.nodeServer,
//         delayTime: 1,
//         env: {
//             'PORT': port,
//             'NODE_ENV': isDev ? 'dev' : 'build'
//         }
//     };
//     watch: [config.sever];
//
//     var nodemon = $.nodemon(nodeOptions);
//         nodemon.on('restart', function (ev) {
//         log('*** nodemon restarted');
//         log('files changed on restart:\n' + ev);
//         })
//         .on('start', function () {
//             log('*** nodemon started');
//             startBrowserSync();
//         })
//         .on('crash', function () {
//             log('*** nodemon crashed: script crashed for some reason');
//         })
//         .on('exit', function () {
//             log('*** nodemon exited cleanly');
//             //nodemon.exit(); // exit so the port is released
//         })
//     ;
// });
//
// function changeEvent(event) {
//     var srcPattern = new RegExp('/.*?=/' + config.source + ')/');
//     log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
// }
//
// function startBrowserSync(){
//     if(browserSync.active){
//         return;
//     }
//
//     log('Starting browser-sync on port ' + port);
//
//     gulp.watch('Starting browser-sync on port ' + port);
//
//     gulp.watch([config.less], ['styles'])
//         .on('change', function (event) {
//             changeEvent(event)
//         });
//
//     var options = {
//         proxy: 'localhost:' + port,
//         port: 3000,
//         files: [config.client + '**/*.*'],
//         ghostNode: {
//             clicks: true,
//             location: false,
//             forms: true,
//             scroll: true
//         },
//         injectChanges: true,
//         logFileChanges: true,
//         logLevel: 'debug',
//         logPrefix: 'gulp-patterns',
//         notify: true,
//         reloadDealy: 1000
//     };
//     browserSync(options);
// }

function errorLoger(error) {
    log('*** Start of Error ***');
    log(error);
    log('*** End of Error ***');
    this.emit('end');
} //

function clean(path, done) {
    log('Cleaning: ' + $.util.colors.blue(path));
    del(path);
    done();  // here we call a callback so the next task waits till this one is finished, it is important when not using pipes in tasks
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}
