'use strict';

angular.module('expenses').controller('CategoriesController', function ($q, $scope, CategoriesService) {

        var newCategoryIsBeeingAdded = false;

        CategoriesService.getCategories().then(function (categories) {
            $scope.items = categories.slice();
        });

        $scope.delete = function (row) {
            CategoriesService.deleteCategory(row).then(function (categories) {
                $scope.items = categories.slice();
            });
        };

        $scope.onRowChanged = function (row) {
            if (!!row.restangularized) {
                return CategoriesService.patchCategory(row);
            } else {
                var d = $q.defer();
                if (!newCategoryIsBeeingAdded) {
                    d.reject();
                    throw new Error("It is not supposed to happen");
                } else {
                    CategoriesService.saveNewCategory(row).then(function (categories) {
                        $scope.items = categories;
                        d.resolve();
                    }, function (problem) {
                        d.reject();
                    });
                }
                return d.promise;
            }
        };

        $scope.addNewRow = function () {
            if (!newCategoryIsBeeingAdded) {
                newCategoryIsBeeingAdded = true;
                $scope.items.push({
                    category: "put name for new category"
                })
            }
        };
    }
);
