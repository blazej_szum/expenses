'use strict';

var app = angular.module('expenses', [
    'smart-table', // table with provided paging
    'ngResource', // for RESTfull calls
    'ngRoute',   // for routing over template
    'xeditable', // nice editable elements in view
    'ngCookies', // for storing data in cookies
    'restangular',
    'ui.bootstrap' // popupover
]);

app.run(function ($rootScope, $location, $cookieStore, editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'

    /* Reset error when a new view is loaded */
    $rootScope.$on('$viewContentLoaded', function () {
        delete $rootScope.error;
    });

    $rootScope.hasRole = function (role) {

        if ($rootScope.user === undefined) {
            return false;
        }

        if ($rootScope.user.roles[role] === undefined) {
            return false;
        }

        return $rootScope.user.roles[role];
    };

    $rootScope.logout = function () {
        delete $rootScope.user;
        delete $rootScope.accessToken;
        $cookieStore.remove('accessToken');
        $location.path("/login");
    };

    /* Try getting valid user from cookie or go to login page */
    var originalPath = $location.path();
    $location.path("/login");
    var accessToken = $cookieStore.get('accessToken');
    if (accessToken !== undefined) {
        $rootScope.accessToken = accessToken;
        // UserService.get(function (user) {
        //     $rootScope.user = user;
        //     $location.path(originalPath);
        // });
    }   

    $rootScope.initialized = true;

});

// Global configuration for Restangular
app.config(['RestangularProvider', function (RestangularProvider) {
    var baseuri = 'expenses/api';
    RestangularProvider.setBaseUrl(baseuri);

    RestangularProvider.setResponseInterceptor(function(data, operation, what, url, response, deferred) {
        if (operation == 'getList') {
            what = url.match( baseuri + "/(.*)/" + "search")[1];
            var resp = data._embedded[what];
            resp._links = data._links;
            return resp
        }
        return data;
    });

    RestangularProvider.setResponseInterceptor(function(data, operation, what, url, response, deferred) {
        if (operation == 'getList') {
            data.forEach(function (element) {
                element.id = Number(element._links.self.href.split('/').pop());
            })
        }
        return data;
    });

    //Using self link for self reference resources, needed for restangular calls
    RestangularProvider.setRestangularFields({
        selfLink: '_links.self.href'
    });
}]);

app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.filter("customCurrency", function (numberFilter) {
  function isNumeric(value) {
    return (!isNaN(parseFloat(value)) && isFinite(value));
  }

  return function (inputNumber, currencySymbol, decimalSeparator, thousandsSeparator, decimalDigits, prefixWithSymbol) {
    if (isNumeric(inputNumber)) {
      // Default values for the optional arguments
      currencySymbol = (typeof currencySymbol === "undefined") ? "$" : currencySymbol;
      decimalSeparator = (typeof decimalSeparator === "undefined") ? "." : decimalSeparator;
      thousandsSeparator = (typeof thousandsSeparator === "undefined") ? "," : thousandsSeparator;
      decimalDigits = (typeof decimalDigits === "undefined" || !isNumeric(decimalDigits)) ? 2 : decimalDigits;
      prefixWithSymbol = (typeof prefixWithSymbol === "undefined") ? true : prefixWithSymbol;

      if (decimalDigits < 0) decimalDigits = 0;

      // Format the input number through the number filter
      // The resulting number will have "," as a thousands separator
      // and "." as a decimal separator.
      var formattedNumber = numberFilter(inputNumber, decimalDigits);

      // Extract the integral and the decimal parts
      var numberParts = formattedNumber.split(".");

      // Replace the "," symbol in the integral part
      // with the specified thousands separator.
      numberParts[0] = numberParts[0].split(",").join(thousandsSeparator);

      // Compose the final result
      var result = numberParts[0];

      if (numberParts.length == 2) {
        result += decimalSeparator + numberParts[1];
      }

      return (prefixWithSymbol ? currencySymbol + " " : "") + result + (prefixWithSymbol ? "" : " " + currencySymbol);
    } else {
      return inputNumber;
    }
  };
});
