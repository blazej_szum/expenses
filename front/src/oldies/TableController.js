'use strict';

angular.module('expenses').controller('TableController', function ($scope, $resource, $route, $location) {
    var tableName = $route.current.$$route.table;
    var table = $resource('rest/tables/' + tableName);

    $scope.pipeToTable = function (tableState) {
        $scope.tableState = tableState;
        var pagination = tableState.pagination;
        var start = pagination.start || 0;
        var number = pagination.number || $scope.itemsByPage;
        table.get({
                page: 1 + (start / number),
                size: number,
                predicate: tableState.sort.predicate,
                reverse: tableState.sort.reverse
            },
            function (pageable) {
                $scope.pageable = pageable;
                countEmotions(pageable.items);
                $scope.items = pageable.items;
                tableState.pagination.numberOfPages = pageable.totalPagesCount;
            });
    };

    function countEmotions(items) {
        items.forEach(function (msg) {
            var approach1 = msg.intensitivityFactor / msg.wordCount * 100;
            var approach2 = msg.emotionsFactor / msg.wordCount * 100;

            if (approach1 < 0) {
                msg.approach1 = "NEGATIVE";
                msg.approach1text = "NEGATIVE: " + approach1.toFixed(0) + "%";
                msg.color1 = "#ffb5b5";
            } else if (approach1 > 0) {
                msg.approach1 = "POSITIVE";
                msg.approach1text = "POSITIVE: " + approach1.toFixed(0) + "%";
                msg.color1 = "#b5f3ff";
            } else {
                msg.approach1 = "NEUTRAL";
                msg.approach1text = "NEUTRAL";
            }

            if (approach2 < 0) {
                msg.approach2 = "NEGATIVE";
                msg.approach2text = "NEGATIVE: " + approach2.toFixed(0) + "%";
                msg.color2 = "#ffb5b5";
            } else if (approach2 > 0) {
                msg.approach2 = "POSITIVE";
                msg.approach2text = "POSITIVE: " + approach2.toFixed(0) + "%";
                msg.color2 = "#b5f3ff";
            } else {
                msg.approach2 = "NEUTRAL";
                msg.approach2text = "NEUTRAL";
            }
        })
    }

    $scope.deleteAllPosts = function () {
        var posts = $resource('rest/posts');
        posts.delete({}, function (result) {
            $.bootstrapGrowl("deleted: " + result.deletedCount + " posts"); // shows growl with information
            reloadTable();
        });

        function reloadTable() {
            $scope.pageable = {};
            $scope.items = {};
            $scope.tableState.pagination.numberOfPages = 1;
        }
    };

    $scope.popup = function (post) {
        console.log("analyse sentence: " + post.message);
        $resource('rest/sentencedesc').get({"sentence": post.message}, function (responce) {
            console.log("responce: " + responce);
            $scope.msgDetails = responce;
        });
    };
});
