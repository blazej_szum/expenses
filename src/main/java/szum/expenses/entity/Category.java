package szum.expenses.entity;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@javax.persistence.Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Calendar additionDate;
    private Calendar lastEditionDate;
    private String category;
    private String categoryGroup;
    private String locationTags;
    private String buyTags;
    private Long userId;
    private Boolean defaultCategory = false;
    private Boolean active = true;

    @OneToMany(mappedBy = "categoryId", fetch = FetchType.LAZY)
    private Set<Expense> expenses = new HashSet<>();

    public Category() {
    }

    public Category(Calendar additionDate, Calendar lastEditionDate,
                    String category, String categoryGroup,
                    String locationTags, String buyTags,
                    Long userId, Boolean defaultCategory) {
        this.defaultCategory = defaultCategory;
        this.additionDate = additionDate;
        this.lastEditionDate = lastEditionDate;
        this.category = category;
        this.locationTags = locationTags;
        this.buyTags = buyTags;
        this.categoryGroup = categoryGroup;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getAdditionDate() {
        return additionDate;
    }

    public void setAdditionDate(Calendar additionDate) {
        this.additionDate = additionDate;
    }

    public Calendar getLastEditionDate() {
        return lastEditionDate;
    }

    public void setLastEditionDate(Calendar lastEditionDate) {
        this.lastEditionDate = lastEditionDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(Set<Expense> expenses) {
        this.expenses = expenses;
    }

    public Boolean getDefaultCategory() {
        return defaultCategory;
    }

    public void setDefaultCategory(Boolean defaultCategory) {
        this.defaultCategory = defaultCategory;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCategoryGroup() {
        return categoryGroup;
    }

    public void setCategoryGroup(String categoryGroup) {
        this.categoryGroup = categoryGroup;
    }

    public Boolean getActive() {
        return active;
    }

    public String getLocationTags() {
        return locationTags;
    }

    public void setLocationTags(String locationTags) {
        this.locationTags = locationTags;
    }

    public String getBuyTags() {
        return buyTags;
    }

    public void setBuyTags(String buyTags) {
        this.buyTags = buyTags;
    }
}
