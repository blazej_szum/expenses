package szum.expenses.entity;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@javax.persistence.Entity
public class AccessToken
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String token;

    @ManyToOne
    private User user;

    private Calendar expiry;

    protected AccessToken(){
        /* Reflection instantiation */
    }

    public AccessToken(User user, String token){
        this.user = user;
        this.token = token;
    }

    public AccessToken(User user, String token, Calendar expiry){
        this(user, token);
        this.expiry = expiry;
    }

    public Long getId()
    {
        return this.id;
    }

    public String getToken()
    {
        return this.token;
    }

    public User getUser()
    {
        return this.user;
    }

    public Calendar getExpiry()
    {
        return this.expiry;
    }

    public boolean isExpired(){
        if (null == this.expiry) {
            return false;
        }

        return this.expiry.getTime().getTime() > System.currentTimeMillis();
    }
}
