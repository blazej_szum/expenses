package szum.expenses.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
public class DefaultCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String categoryGroup;
    private String category;
    private String locationTags;
    private String buyTags;
    private Calendar additionDate;
    private Calendar lastEditionDate;

    public DefaultCategory() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Calendar getAdditionDate() {
        return additionDate;
    }

    public void setAdditionDate(Calendar additionDate) {
        this.additionDate = additionDate;
    }

    public Calendar getLastEditionDate() {
        return lastEditionDate;
    }

    public void setLastEditionDate(Calendar lastEditionDate) {
        this.lastEditionDate = lastEditionDate;
    }

    public String getCategoryGroup() {
        return categoryGroup;
    }

    public void setCategoryGroup(String categoryGroup) {
        this.categoryGroup = categoryGroup;
    }

    public String getLocationTags() {
        return locationTags;
    }

    public void setLocationTags(String locationTags) {
        this.locationTags = locationTags;
    }

    public String getBuyTags() {
        return buyTags;
    }

    public void setBuyTags(String buyTags) {
        this.buyTags = buyTags;
    }
}