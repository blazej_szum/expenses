package szum.expenses.entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

//@javax.persistence.Entity
public enum Role implements GrantedAuthority{
    USER("ROLE_USER"),
    ADMIN("ROLE_ADMIN");

    private String authority;

    Role(){}

    Role(String authority)
    {
        this.authority = authority;
    }

    @Override
    public String getAuthority()
    {
        return this.authority;
    }
}
