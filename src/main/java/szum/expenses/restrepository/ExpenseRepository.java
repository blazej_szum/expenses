package szum.expenses.restrepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import szum.expenses.entity.Expense;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public interface ExpenseRepository extends PagingAndSortingRepository<Expense, Long> {
    	Page<Expense> findByUserId(@Param("userId") Long userId, Pageable pageable);
    	Page<Expense> findByUserIdAndCategoryId(Long userId, Long categoryId, Pageable pageable);
    	Page<Expense> findByUserIdAndAdditionDateBetween(Long id, Date dateFrom, Date dateTo, Pageable pageable);
    	Page<Expense> findByUserIdAndMoneyBetween(Long id, BigInteger moneyFrom, BigInteger moneyTo, Pageable pageable);
    	Page<Expense> findByUserIdAndCategoryIdInAndAdditionDateBetweenAndMoneyBetween(Long userId, Collection<Long> categoryIds, Calendar dateFrom, Calendar dateTo, BigDecimal moneyFrom, BigDecimal moneyTo, Pageable pageable);
}