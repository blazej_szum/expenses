package szum.expenses.restrepository;

import org.springframework.data.repository.CrudRepository;
import szum.expenses.entity.AccessToken;

public interface AccessTokenRepository extends CrudRepository<AccessToken, Long> {

    AccessToken findByToken(String token);
}