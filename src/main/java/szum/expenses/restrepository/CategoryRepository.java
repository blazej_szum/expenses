package szum.expenses.restrepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import szum.expenses.entity.Category;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
    Page<Category> findByUserId(@Param("userId") Long id, Pageable pageable);
    Page<Category> findByUserIdOrderByCategoryGroup(@Param("userId") Long id, Pageable pageable);
}