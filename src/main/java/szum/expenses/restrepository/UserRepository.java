package szum.expenses.restrepository;

import org.springframework.data.repository.CrudRepository;
import szum.expenses.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByLogin(String name);
}