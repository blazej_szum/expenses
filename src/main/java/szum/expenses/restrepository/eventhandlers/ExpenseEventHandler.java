package szum.expenses.restrepository.eventhandlers;

import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import szum.expenses.entity.Expense;

import java.util.Calendar;

/**
 * Created by blazej on 12.03.17.
 */
@RepositoryEventHandler(Expense.class)
public class ExpenseEventHandler {

    @HandleBeforeCreate
    public void setCreationDateForExpense(Expense expense){
        expense.setLastEditionDate(Calendar.getInstance());
        expense.setAdditionDate(Calendar.getInstance());
    }
}