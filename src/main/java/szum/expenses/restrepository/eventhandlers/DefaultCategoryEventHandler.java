package szum.expenses.restrepository.eventhandlers;

import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import szum.expenses.entity.DefaultCategory;

import java.util.Calendar;

/**
 * Created by blazej on 12.03.17.
 */
@RepositoryEventHandler(DefaultCategory.class)
public class DefaultCategoryEventHandler {

    @HandleBeforeCreate
    public void setCreationDateForCategory(DefaultCategory cateogry){
        cateogry.setAdditionDate(Calendar.getInstance());
        cateogry.setLastEditionDate(Calendar.getInstance());
    }
}