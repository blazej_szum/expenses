package szum.expenses.restrepository.eventhandlers;

import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import szum.expenses.entity.Category;

import java.util.Calendar;

/**
 * Created by blazej on 12.03.17.
 */
@RepositoryEventHandler(Category.class)
public class CategoryEventHandler {

    @HandleBeforeCreate
    public void setCreationDateForCategory(Category cateogry){
        cateogry.setAdditionDate(Calendar.getInstance());
        cateogry.setLastEditionDate(Calendar.getInstance());
    }
}