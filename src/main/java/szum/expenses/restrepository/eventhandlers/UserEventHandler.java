package szum.expenses.restrepository.eventhandlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import szum.expenses.entity.Category;
import szum.expenses.entity.DefaultCategory;
import szum.expenses.entity.Role;
import szum.expenses.entity.User;
import szum.expenses.restrepository.CategoryRepository;
import szum.expenses.restrepository.DefaultCategoryRepository;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.StreamSupport;
/**
 * Created by blazej on 12.03.17.
 */
@RepositoryEventHandler(User.class)
public class UserEventHandler {

    @Autowired
    private DefaultCategoryRepository dcr;

    @Autowired
    private CategoryRepository cr;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @HandleBeforeCreate(User.class)
    public void setRoleForCommonUser(User user) {
        user.addRole(Role.USER);
    }

    @HandleAfterCreate(User.class)
    public void addDefaultCategoriesForUser(User user) {

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Iterable<DefaultCategory> dcs = dcr.findAll();
        Set<Category> userCategories = new HashSet<>();

        StreamSupport.stream(dcs.spliterator(), false).forEach(dc -> {
            userCategories.add(new Category(Calendar.getInstance(),
                    Calendar.getInstance(), dc.getCategory(), dc.getCategoryGroup(),
                    dc.getLocationTags(), dc.getBuyTags()
                    , user.getId(), true));
        });

        cr.save(userCategories);
    }
}