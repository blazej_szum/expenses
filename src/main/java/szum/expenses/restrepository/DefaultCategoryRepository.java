package szum.expenses.restrepository;

import org.springframework.data.repository.CrudRepository;

import szum.expenses.entity.DefaultCategory;

public interface DefaultCategoryRepository extends CrudRepository<DefaultCategory, Long> {}
