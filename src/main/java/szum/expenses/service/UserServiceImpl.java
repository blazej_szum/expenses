package szum.expenses.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szum.expenses.entity.AccessToken;
import szum.expenses.entity.User;
import szum.expenses.restrepository.AccessTokenRepository;
import szum.expenses.restrepository.UserRepository;

import java.util.UUID;


@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private AccessTokenRepository accessTokenRepo;

    protected UserServiceImpl(){}

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        return this.userRepo.findByLogin(username);
    }

    @Override
    @Transactional
    public User findUserByAccessToken(String accessTokenString){

        AccessToken accessToken = accessTokenRepo.findByToken(accessTokenString);

        if (null == accessToken) {
            return null;
        }

        if (accessToken.isExpired()) {
            accessTokenRepo.delete(accessToken);
            return null;
        }

        return accessToken.getUser();
    }

    @Override
    @Transactional
    public AccessToken createAccessToken(User user){
        AccessToken accessToken = new AccessToken(user, UUID.randomUUID().toString());
        return accessTokenRepo.save(accessToken);
    }
}
