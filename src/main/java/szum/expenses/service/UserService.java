package szum.expenses.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import szum.expenses.entity.AccessToken;
import szum.expenses.entity.User;

public interface UserService extends UserDetailsService
{
    User findUserByAccessToken(String accessToken);

    AccessToken createAccessToken(User user);
}
