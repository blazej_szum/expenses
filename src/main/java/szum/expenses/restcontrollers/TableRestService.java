package szum.expenses.restcontrollers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("tables")
@Service
public class TableRestService {

    @PersistenceContext
    private EntityManager entityManager;

    @PreAuthorize("hasRole('ROLE_USER')") // TODO: przetestować jak to działa
    @GET
	@Path("/{tablename}")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getTable(
          @PathParam("tablename") String tableName,
		  @DefaultValue("1")@QueryParam("page") Integer page,
		  @DefaultValue("10")@QueryParam("size") Integer size,
		  @QueryParam("predicate") String predicate,
          @QueryParam("reverse") Boolean reverse){

        StringBuilder queryString = new StringBuilder();
        queryString.append("From " + tableName);
        if(predicate!=null && reverse!=null){
            queryString.append(" order by ").append(predicate);
            if(reverse){
                queryString.append(" asc");
            }else{
                queryString.append(" desc");
            }
		}

        Query query = entityManager.createQuery(queryString.toString());
        query.setFirstResult((page-1)*size);
        query.setMaxResults(size);
        List items = query.getResultList();

        query = entityManager.createQuery ("SELECT count(x) FROM " + tableName + " x");
        long totalCount = (Long) query.getSingleResult();
        int totalPagesCount = (int)Math.ceil((double)totalCount/(double)size);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("items", items);
        result.put("totalPagesCount", totalPagesCount);
        System.out.println("totalCount:" + totalCount );
        return Response.ok().entity(result).build();
	}
}
