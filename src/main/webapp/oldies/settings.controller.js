'use strict';

angular.module('expenses').controller('TableController', function ($scope, $resource, $route, $location, Restangular) {
    var tableName = $route.current.$$route.table;
    var table = $resource('rest/tables/' + tableName);

    $scope.pipeToTable = function (tableState) {
        $scope.tableState = tableState;
        var pagination = tableState.pagination;
        var start = pagination.start || 0;
        var number = pagination.number || $scope.itemsByPage;
        table.get({
                page: 1 + (start / number),
                size: number,
                predicate: tableState.sort.predicate,
                reverse: tableState.sort.reverse
            },
            function (pageable) {
                $scope.pageable = pageable;
                countEmotions(pageable.items);
                $scope.items = pageable.items;
                tableState.pagination.numberOfPages = pageable.totalPagesCount;
            });
    };


    $scope.delete = function (row) {

        Restangular.one("")

        var posts = $resource('rest/posts');
        posts.delete({}, function (result) {
            $.bootstrapGrowl("deleted: " + result.deletedCount + " posts"); // shows growl with information
            reloadTable();
        });

        function reloadTable() {
            $scope.pageable = {};
            $scope.items = {};
            $scope.tableState.pagination.numberOfPages = 1;
        }
    };

    $scope.popup = function (post) {
        console.log("analyse sentence: " + post.message);
        $resource('rest/sentencedesc').get({"sentence": post.message}, function (responce) {
            console.log("responce: " + responce);
            $scope.msgDetails = responce;
        });
    };
});
