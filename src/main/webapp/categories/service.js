'use strict';

angular
    .module('expenses')
    .factory('CategoriesService', function ($rootScope, $q, $timeout, RepoBuilder) {
        var self = this,

            categoriesRepo = RepoBuilder.build("categories"),
            categories;

        function getActualCategories() {
            return categoriesRepo
                .getList('findByUserId', {userId: $rootScope.user.id})
                .then(function (response) {
                    categories = response;
                    return categories;
                }, function (problem) {
                    $.bootstrapGrowl("problem loading table content");
                    return problem;
                });
        }

        angular.extend(self, {
            getCategories: function  () {
                if (categories) {
                    return $q(function (resolve) {
                        $timeout(function () {
                            resolve(categories);
                        });
                    });
                } else {
                    return getActualCategories();
                }
            },
            patchCategory: function (row) {
                var d = $q.defer();
                row.patch().then(function () {
                    $.bootstrapGrowl("updated category: " + row.category);
                    d.resolve();
                }, function () {
                    $.bootstrapGrowl("problem updating: " + row.category);
                    d.reject();
                });
                return d.promise;
            },

            saveNewCategory: function (newCategory) {
                newCategory.userId = $rootScope.user.id;
                return categoriesRepo.saveNew(newCategory).then(function () {
                    $.bootstrapGrowl("added new category: " + newCategory.category);
                    return getActualCategories();
                }, function (problem) {
                    $.bootstrapGrowl("problem adding new category: " + newCategory.category);
                    return $q.defer().reject(problem).promise;
                });
            },

            deleteCategory: function (row) {
                return row.remove().then(function (responce) {
                    $.bootstrapGrowl("removed cateogory: " + row.category);
                    return getActualCategories();
                }, function (problem) {
                    $.bootstrapGrowl("problem removing category: " + row.category);
                    return $q.defer.reject(problem).promise;
                });
            }
        });

        return self;
    });