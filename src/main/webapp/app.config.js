'use strict';

// ui-router ?
//api check
angular.module('expenses').config(['$routeProvider', '$locationProvider', '$httpProvider',
    function ($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider.when('/expenses', {
            templateUrl: 'expense/expenses.html',
            controller: 'ExpensesController'
        }).when('/categories', {
            templateUrl: 'categories/categories.html',
            controller: 'CategoriesController'
        }).when('/settings', {
            templateUrl: 'oldies/settings.html',
            controller: 'SettingsController'
        }).when('/login', {
            templateUrl: 'login/login.html',
            controller: 'LoginController'
        }).otherwise({
            redirectTo: '/login'
        });

        // $locationProvider.hashPrefix('!');

        /* Register error provider that shows message on failed requests or redirects to login page on
         * unauthenticated requests */
        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
                return {
                    'responseError': function (rejection) {
                        var status = rejection.status;
                        var config = rejection.config;
                        var method = config.method;
                        var url = config.url;

                        if (status == 401) {
                            $location.path("/login");
                        } else {
                            $rootScope.error = method + " on " + url + " failed with status " + status;
                        }

                        return $q.reject(rejection);
                    }
                };
            }
        );

        /* Registers auth token interceptor, auth token is either passed by header or by query parameter
         * as soon as there is an authenticated user */
        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
                return {
                    'request': function (config) {
                        var isRestCall = (config.url.indexOf('rest') >= 0 );
                        if (isRestCall && angular.isDefined($rootScope.accessToken)) {
                            var accessToken = $rootScope.accessToken;
                            config.headers['X-Access-Token'] = accessToken;
                        }
                        return config || $q.when(config);
                    }
                };
            }
        );
    }
]);
