'use strict';

angular.module('expenses').controller('LoginController', function ($httpParamSerializer, $http, $scope, $rootScope, $location, $cookieStore, RepoBuilder, Restangular) {

        // var authenticate = Restangular.all("authenticate");

        $scope.rememberMe = false;

    $scope.login = function () {
        $http({
            method: 'POST',
            url: 'auth/authenticate',
            data: $httpParamSerializer({
                username: $scope.username,
                password: $scope.password
            }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (result) {
                $rootScope.user = result.data.user;                
                $rootScope.accessToken = result.data.token;
                if ($scope.rememberMe) {
                    $cookieStore.put('accessToken', result.data.token);
                }
            }, function (error) {
                $.bootstrapGrowl("problem logging in, error: " + angular.toJson(error));
            });
        };

        $scope.register = function () {
            Restangular.all('users')
                .post({
                    login: $scope.registerUsername,
                    email: $scope.registerEmail,
                    password: $scope.registerPass1
                }).then(function () {
                $.bootstrapGrowl("successfully registered: " + $scope.registerUsername);
            }, function () {
                $.bootstrapGrowl("problem registering: " + $scope.registerUsername);
            });
        };
    });
