'use strict';

angular.module('expenses').controller('ExpensesController', function ($rootScope, $q, $scope, RepoBuilder, CategoriesService, $filter) {

    CategoriesService.getCategories().then(function (categories) {
        $scope.categories = categories;
    });

    $scope.showStatus = function (expense) {
        var selected = $filter('filter')($scope.categories, {id: expense.categoryId});
        return (expense.categoryId && selected.length) ? selected[0].category : 'Not set';
    };

    var expenses = RepoBuilder.build("expenses");

    function loadTable() {
        expenses.getList('findByUserId', {userId: $rootScope.user.id}).then(function (responce) {
            $scope.items = responce;
        }, function () {
            $.bootstrapGrowl("problem loading table content");
        });
    }

    loadTable();

    $scope.delete = function (row) {
        row.remove().then(function (responce) {
            $.bootstrapGrowl("removed expense");
            loadTable();
        }, function () {
            $.bootstrapGrowl("problem removing expense");
        });
    };

    $scope.saveExpense = function (row, id) {
        var d = $q.defer();
        if (!!row.restangularized) {
            patch(row, d);
        } else {
            saveNew(row, d);
        }
        return d.promise;
    };

    $scope.change = function (row) {
        var d = $q.defer();
        if (!!row.restangularized) {
            patch(row, d);
        } else {
            saveNew(row, d);
        }
        return d.promise;
    };

    function patch(row, d) {
        row.patch().then(function (responce) {
            $.bootstrapGrowl("updated expense: " + responce.money + " of category: " + responce.categoryId);
            d.resolve();
        }, function () {
            $.bootstrapGrowl("problem updating expense");
            d.reject();
        });
    }

    function saveNew(row, d) {
        if (addingNew == false) {
            throw new Error("It is not supposed to happen");
            d.reject();
        } else {
            row.userId = $rootScope.user.id;
            expenses.saveNew(row).then(function (responce) {
                $.bootstrapGrowl("added new expense: " + responce.money + " of category: " + responce.categoryId);
                d.resolve();
            }, function () {
                $.bootstrapGrowl("problem adding new expense");
                d.reject();
            }).finally(function () {
                addingNew = false;
                loadTable();
            });
        }
    }

    var addingNew = false;

    $scope.addNewRow = function () {
        if (addingNew == false) {
            addingNew = true;
            $scope.inserted = {
                categoryId: "what category?",
                money: "how much?"
            };
            $scope.items.push($scope.inserted)
        }
    };

    $scope.removeRow = function(index){
        $scope.items.splice(index,1);
        if(addingNew == false){
            throw new Error("it is not supposed to happened");
        }
        addingNew = false;
    }
});
