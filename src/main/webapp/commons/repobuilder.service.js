'use strict';

angular.module('expenses').factory('RepoBuilder', function (Restangular) {

    function SearchEngine(context){
        var self = this;
        self.one = Restangular.one(context+"/search");
        self.all = Restangular.all(context);
        return self;
    }

    SearchEngine.prototype = {
        rest: function () {
          return this.one;
        },
        getList: function (query,params) {
          return this.one.getList(query,params);
        },
        saveNew: function (entity) {
          return this.all.doPOST(entity);
        }
    };

    SearchEngine.build = function(context){
        return new SearchEngine(context);
    };

    return SearchEngine;

});
