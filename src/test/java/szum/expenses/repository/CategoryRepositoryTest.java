package szum.expenses.repository;

import com.google.common.collect.Iterators;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import szum.expenses.restrepository.CategoryRepository;
import szum.expenses.restrepository.UserRepository;
import szum.expenses.entity.Category;
import szum.expenses.entity.User;
import szum.expenses.entity.generators.CategoryBuilder;
import szum.expenses.entity.generators.UserBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
@WebAppConfiguration
public class CategoryRepositoryTest {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected CategoryRepository catRepo;

    @Autowired
    protected UserRepository userRepo;

    @Autowired
    protected CategoryBuilder cb;

    @Before
    public void configureMockMvcInstance() {
        RestAssuredMockMvc.webAppContextSetup(wac);
    }

    @Before
    @After
    public void cleanDatabase() {
        catRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void find_by_test() {

        User usr1 = userRepo.save(UserBuilder.newBuilder().withLogin("usr1").build());
        catRepo.save(cb.withUserId(usr1.getId()).withCategory("category1").build());
        catRepo.save(cb.withUserId(usr1.getId()).withCategory("category2").build());
        catRepo.save(cb.withUserId(usr1.getId()).withCategory("category3").build());
        catRepo.save(cb.withUserId(usr1.getId()).withCategory("category4").build());
        catRepo.save(cb.withUserId(usr1.getId()).withCategory("category5").build());

        User usr2 = userRepo.save(UserBuilder.newBuilder().withLogin("usr2").build());
        catRepo.save(cb.withUserId(usr2.getId()).withCategory("category10").build());
        catRepo.save(cb.withUserId(usr2.getId()).withCategory("category20").build());
        catRepo.save(cb.withUserId(usr2.getId()).withCategory("category30").build());
        catRepo.save(cb.withUserId(usr2.getId()).withCategory("category40").build());
        catRepo.save(cb.withUserId(usr2.getId()).withCategory("category50").build());

        Page<Category> page = catRepo.findByUserId(usr1.getId(), new PageRequest(0,5));
        assertEquals("wrong number of records",5,Iterators.size(page.iterator()));

        page = catRepo.findByUserId(usr2.getId(), new PageRequest(0,3));
        assertEquals("wrong number of records",3,Iterators.size(page.iterator()));

        page = catRepo.findByUserId(usr2.getId(), new PageRequest(0,2,new Sort(Sort.Direction.ASC, "category")));
        assertEquals("wrong answer","category10",page.getContent().get(0).getCategory());

        page = catRepo.findByUserId(usr2.getId(), new PageRequest(0,2,new Sort(Sort.Direction.DESC, "category")));
        assertEquals("wrong answer","category50",page.getContent().get(0).getCategory());
    }

}