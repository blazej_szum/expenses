package szum.expenses.repository;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import szum.expenses.restrepository.UserRepository;
import szum.expenses.entity.User;
import szum.expenses.entity.generators.UserBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
@WebAppConfiguration
public class UserRepositoryTest {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected UserRepository userRepo;

    @Autowired
    protected UserBuilder userBuilder;

    @Before
    public void configureMockMvcInstance() {
        RestAssuredMockMvc.webAppContextSetup(wac);
    }

    @Before
    public void initDatabase() {
        userRepo.deleteAll();
    }

    @After
    public void cleanDatabase() {
        userRepo.deleteAll();
    }

    @Test
    public void find_by_name_test() {

        User user = userRepo.save(userBuilder.withLogin("login").withPassword("pass").withEmail("email").build());
        User user1 = userRepo.findByLogin("login");

        assertNotNull("no user found", user1);
        assertEquals("email mismatch", "email", user1.getEmail());
    }
}