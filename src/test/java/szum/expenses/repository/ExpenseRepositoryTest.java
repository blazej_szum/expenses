package szum.expenses.repository;

import com.google.common.collect.Iterators;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import szum.expenses.Utils;
import szum.expenses.entity.Category;
import szum.expenses.entity.User;
import szum.expenses.entity.generators.CategoryBuilder;
import szum.expenses.entity.generators.ExpenseBuilder;
import szum.expenses.entity.generators.UserBuilder;
import szum.expenses.restrepository.CategoryRepository;
import szum.expenses.restrepository.ExpenseRepository;
import szum.expenses.restrepository.UserRepository;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
@WebAppConfiguration
public class ExpenseRepositoryTest {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected CategoryRepository catRepo;

    @Autowired
    protected UserRepository userRepo;

    @Autowired
    protected ExpenseRepository expensesRepo;

    @Autowired
    protected CategoryBuilder cb;

    @Autowired
    protected ExpenseBuilder eb;

    @Before
    public void configureMockMvcInstance() {
        RestAssuredMockMvc.webAppContextSetup(wac);
    }

    @After
    @Before
    public void cleanDatabase() {
        expensesRepo.deleteAll();
        catRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void find_by_category_test() {

        User usr1 = userRepo.save(UserBuilder.newBuilder().withLogin("usr1").build());
        User usr2 = userRepo.save(UserBuilder.newBuilder().withLogin("usr1").build());

        Category cat1 = catRepo.save(cb.withUserId(usr1.getId()).withCategory("category1").build());
        Category cat2 = catRepo.save(cb.withUserId(usr1.getId()).withCategory("category2").build());
        Category cat3 = catRepo.save(cb.withUserId(usr2.getId()).withCategory("category2").build());

        expensesRepo.save(eb.withUserId(usr1.getId()).withCategoryId(cat1.getId()).withAdditionDate(Utils.NewDate(2010,2,1)).withMoney(new BigDecimal("1.00")).build());
        expensesRepo.save(eb.withUserId(usr1.getId()).withCategoryId(cat2.getId()).withAdditionDate(Utils.NewDate(2010,3,1)).withMoney(new BigDecimal("1.00")).build());
        expensesRepo.save(eb.withUserId(usr1.getId()).withCategoryId(cat2.getId()).withAdditionDate(Utils.NewDate(2010,4,1)).withMoney(new BigDecimal("2.00")).build());
        expensesRepo.save(eb.withUserId(usr2.getId()).withCategoryId(cat3.getId()).withAdditionDate(Utils.NewDate(2010,5,1)).withMoney(new BigDecimal("2.00")).build());
        expensesRepo.save(eb.withUserId(usr2.getId()).withCategoryId(cat3.getId()).withAdditionDate(Utils.NewDate(2010,6,1)).withMoney(new BigDecimal("3.00")).build());
        expensesRepo.save(eb.withUserId(usr2.getId()).withCategoryId(cat3.getId()).withAdditionDate(Utils.NewDate(2010,7,1)).withMoney(new BigDecimal("3.00")).build());

        assertEquals("bad search with user id",3,Iterators.size(expensesRepo.findByUserId(usr1.getId(), new PageRequest(0,10)).iterator()));
        assertEquals("bad search with user id and category id",2,Iterators.size(expensesRepo.findByUserIdAndCategoryId(usr1.getId(),cat2.getId(), new PageRequest(0,10)).iterator()));
        assertEquals("bad search with all criteria",3,Iterators.size(
                expensesRepo.findByUserIdAndCategoryIdInAndAdditionDateBetweenAndMoneyBetween(
                        usr2.getId(),
                        Arrays.asList(cat3.getId()),
                        Utils.NewDate(2009,5,1),
                        Utils.NewDate(2011,5,1),
                        new BigDecimal("0.09"),
                        new BigDecimal("4.00"),
                        new PageRequest(0,10)).iterator()));

         assertEquals("bad search with date",0,Iterators.size(
                expensesRepo.findByUserIdAndCategoryIdInAndAdditionDateBetweenAndMoneyBetween(
                        usr2.getId(),
                        Arrays.asList(cat3.getId()),
                        Utils.NewDate(2009,4,0),
                        Utils.NewDate(2009,6,2),
                        new BigDecimal("0.09"),
                        new BigDecimal("4.00"),
                        new PageRequest(0,10)).iterator()));

         assertEquals("bad search with date",2,Iterators.size(
                expensesRepo.findByUserIdAndCategoryIdInAndAdditionDateBetweenAndMoneyBetween(
                        usr2.getId(),
                        Arrays.asList(cat3.getId()),
                        Utils.NewDate(2009,4,0),
                        Utils.NewDate(2010,6,2),
                        new BigDecimal("0.09"),
                        new BigDecimal("4.00"),
                        new PageRequest(0,10)).iterator()));

        assertEquals("bad search with money",1,Iterators.size(
                expensesRepo.findByUserIdAndCategoryIdInAndAdditionDateBetweenAndMoneyBetween(
                        usr2.getId(),
                        Arrays.asList(cat3.getId()),
                        Utils.NewDate(2009,5,1),
                        Utils.NewDate(2011,5,1),
                        new BigDecimal("1.09"),
                        new BigDecimal("2.50"),
                        new PageRequest(0,10)).iterator()));

        assertEquals("bad search with money",2,Iterators.size(
                expensesRepo.findByUserIdAndCategoryIdInAndAdditionDateBetweenAndMoneyBetween(
                        usr2.getId(),
                        Arrays.asList(cat3.getId()),
                        Utils.NewDate(2009,5,1),
                        Utils.NewDate(2011,5,1),
                        new BigDecimal("2.09"),
                        new BigDecimal("3.50"),
                        new PageRequest(0,10)).iterator()));
    }

}