package szum.expenses.entity.generators;

abstract class Builder<T> {

    private Class<T> clazz;
    protected T instance;

    public Builder() {}

    protected Builder(Class<T> clazz){
        super();
        this.clazz = clazz;
        this.instance = createNewInstance();
    }

    public  T build(){
        T buildInstance = this.instance;
        this.instance = createNewInstance();
        return buildInstance;
    }

    private T createNewInstance(){
        try {
            return this.clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
