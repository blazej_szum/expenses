package szum.expenses.entity.generators;

import org.springframework.stereotype.Component;
import szum.expenses.entity.DefaultCategory;
import szum.expenses.entity.User;

import java.util.Date;

@Component
public class UserBuilder extends Builder<User>{

    public static UserBuilder newBuilder(){
        return new UserBuilder();
    };

    private UserBuilder(){
        super(User.class);
    }

    public UserBuilder withId(Long id){
        instance.setId(id);
        return this;
    };

    public UserBuilder withPassword(String password){
        instance.setPassword(password);
        return this;
    };

    public UserBuilder withLogin(String login){
        instance.setLogin(login);
        return this;
     };

    public UserBuilder withEmail(String email){
        instance.setEmail(email);
        return this;
     };
}