package szum.expenses.entity.generators;

import org.springframework.stereotype.Component;
import szum.expenses.entity.DefaultCategory;

import java.util.Calendar;
import java.util.Date;

@Component
public class DefaultCategoryBuilder extends Builder<DefaultCategory>{

    public static DefaultCategoryBuilder newBuilder(){
        return new DefaultCategoryBuilder();
    };

    private DefaultCategoryBuilder(){
        super(DefaultCategory.class);
    }

    public DefaultCategoryBuilder withId(Long id){
        instance.setId(id);
        return this;
    };
    public DefaultCategoryBuilder withCategory(String category){
        instance.setCategory(category);
        return this;
    };

    public DefaultCategoryBuilder withAdditionDate(Calendar additionDate){
        instance.setAdditionDate(additionDate);
        return this;
     };

    public DefaultCategoryBuilder withLastEditionDate(Calendar lastEditionDate){
        instance.setLastEditionDate(lastEditionDate);
        return this;
    };

}