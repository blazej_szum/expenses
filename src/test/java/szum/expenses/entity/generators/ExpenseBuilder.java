package szum.expenses.entity.generators;

import org.springframework.stereotype.Component;
import szum.expenses.entity.Category;
import szum.expenses.entity.Expense;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

@Component
public class ExpenseBuilder extends Builder<Expense>{

    public static ExpenseBuilder newBuilder(){
        return new ExpenseBuilder();
    };

    private ExpenseBuilder(){
        super(Expense.class);
    }

    public ExpenseBuilder withId(Long id){
        instance.setId(id);
        return this;
    };

    public ExpenseBuilder withCategoryId(Long categoryId){
        instance.setCategoryId(categoryId);
        return this;
    };

    public ExpenseBuilder withUserId(Long id){
        instance.setUserId(id);
        return this;
     };

    public ExpenseBuilder withAdditionDate(Calendar date){
        instance.setAdditionDate(date);
        return this;
     };

    public ExpenseBuilder withLastEditionDate(Calendar date){
        instance.setLastEditionDate(date);
        return this;
     };

     public ExpenseBuilder withMoney(BigDecimal money){
        instance.setMoney(money);
        return this;
     };
}