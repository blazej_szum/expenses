package szum.expenses.entity.generators;

import org.springframework.stereotype.Component;
import szum.expenses.entity.Category;
import szum.expenses.entity.User;

import java.util.Calendar;
import java.util.Date;

@Component
public class CategoryBuilder extends Builder<Category>{

    public static CategoryBuilder newBuilder(){
        return new CategoryBuilder();
    };

    private CategoryBuilder(){
        super(Category.class);
    }

    public CategoryBuilder withId(Long id){
        instance.setId(id);
        return this;
    };

    public CategoryBuilder withCategory(String category){
        instance.setCategory(category);
        return this;
    };

    public CategoryBuilder withUserId(Long id){
        instance.setUserId(id);
        return this;
     };

    public CategoryBuilder withAdditionDate(Calendar date){
        instance.setAdditionDate(date);
        return this;
     };

    public CategoryBuilder withLastEditionDate(Calendar date){
        instance.setLastEditionDate(date);
        return this;
     };
}