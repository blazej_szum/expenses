package szum.expenses.restcontrollers;

import io.restassured.http.ContentType;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import szum.expenses.entity.DefaultCategory;
import szum.expenses.entity.generators.DefaultCategoryBuilder;
import szum.expenses.restrepository.DefaultCategoryRepository;

import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
@WebAppConfiguration
public class DefaultCategoryRepositoryTest {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected DefaultCategoryRepository catRepo;

    @Autowired
    protected DefaultCategoryBuilder categoryBuilder;

    @Before
    public void configureMockMvcInstance() {
        RestAssuredMockMvc.webAppContextSetup(wac);
    }

    @Before
    public void initDatabase() {
        catRepo.deleteAll();
    }

    @After
    public void cleanDatabase() {
        catRepo.deleteAll();
    }

    @Test
    public void simple_select_all() {

        catRepo.save(categoryBuilder.newBuilder()
                .withCategory("first_category")
                .withAdditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .withLastEditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .build());

        catRepo.save(categoryBuilder.newBuilder()
                .withCategory("second_category")
                .withAdditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .withLastEditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .build());

        RestAssuredMockMvc.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/defaultCategories")
                .then()
                .assertThat().body("_embedded.defaultCategories.category", hasItems("first_category","second_category"))
                .statusCode(HttpURLConnection.HTTP_OK); //200
    }

    @Test
    public void simple_insert() {

        Map<String, String> body = new HashMap<>();
        body.put("lastEditionDate", "2015-08-02T4:50:49");
        body.put("additionDate", "2000-01-02T1:00:00");

        RestAssuredMockMvc.given().
                contentType(ContentType.JSON).
                body(body).
                when().
                post("/defaultCategories").
                then().
                statusCode(HttpURLConnection.HTTP_CREATED); //201
    }

    @Test
    public void simple_update_put() {

        DefaultCategory defaultCategory = catRepo.save(categoryBuilder.newBuilder()
                .withCategory("third_category")
                .withAdditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .withLastEditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .build());

        Map<String, String> body = new HashMap<>();
        body.put("category", "updated_category");
        body.put("lastEditionDate", "2012-02-21T14:00:00");
        body.put("additionDate", "2012-02-21T14:00:00");
        RestAssuredMockMvc.given().
                contentType(ContentType.JSON).
                body(body).
                when().
                put("/defaultCategories/" + defaultCategory.getId()).
                then().
                statusCode(new Matcher<Integer>() {
                    @Override
                    public boolean matches(Object o) {
                        Integer code = (Integer) o;
                        if (code == HttpURLConnection.HTTP_OK) {
                            return true;
                        } else if (code == HttpURLConnection.HTTP_NO_CONTENT){
                            return true;
                    }else{
                            return false;
                        }
                    }

                    @Override
                    public void describeMismatch(Object o, Description description) {

                    }

                    @Override
                    public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {

                    }

                    @Override
                    public void describeTo(Description description) {

                    }
                });

        defaultCategory = catRepo.findOne(defaultCategory.getId());

        assertEquals("entity has not been updated","updated_category", defaultCategory.getCategory());

        RestAssuredMockMvc.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/defaultCategories/" + defaultCategory.getId())
                .then()
                .assertThat()
                .body("category", equalTo("updated_category"))
                .body("lastEditionDate", equalTo("2012-02-21T14:00:00.000+0000"))
                .body("additionDate", equalTo("2012-02-21T14:00:00.000+0000"))
                .statusCode(HttpURLConnection.HTTP_OK); //200
    }

    @Test
    public void simple_update_patch() {

        DefaultCategory defaultCategory = catRepo.save(categoryBuilder.newBuilder()
                .withCategory("fourth_category")
                .withAdditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .withLastEditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .build());

        Map<String, String> body = new HashMap<>();
        body.put("category", "updated_category2");
        body.put("lastEditionDate", "2015-08-02T04:50:49");
        body.put("additionDate", "2000-01-02T01:00:00");
        RestAssuredMockMvc.given().
                contentType(ContentType.JSON).
                body(body).
                when().
                patch("/defaultCategories/" + defaultCategory.getId()).
                then().
                statusCode(new Matcher<Integer>() {
                    @Override
                    public boolean matches(Object o) {
                        Integer code = (Integer) o;
                        if (code == HttpURLConnection.HTTP_OK) {
                            return true;
                        } else if (code == HttpURLConnection.HTTP_NO_CONTENT){
                            return true;
                    }else{
                            return false;
                        }
                    }

                    @Override
                    public void describeMismatch(Object o, Description description) {

                    }

                    @Override
                    public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {

                    }

                    @Override
                    public void describeTo(Description description) {

                    }
                });

         defaultCategory = catRepo.findOne(defaultCategory.getId());

        assertEquals("entity has not been updated","updated_category2", defaultCategory.getCategory());

        RestAssuredMockMvc.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/defaultCategories/" + defaultCategory.getId())
                .then()
                .assertThat()
                .body("category", equalTo("updated_category2"))
                .body("lastEditionDate", equalTo("2015-08-02T04:50:49.000+0000"))
                .body("additionDate", equalTo("2000-01-02T01:00:00.000+0000"))
                .statusCode(HttpURLConnection.HTTP_OK); //200
    }

     @Test
    public void simple_delete() {

        DefaultCategory defaultCategory = catRepo.save(categoryBuilder.newBuilder()
                .withCategory("next_category")
                .withAdditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .withLastEditionDate(new Calendar.Builder().setDate(2012, 2, 21).setTimeOfDay(14, 0, 0).build())
                .build());

         long defCatId = defaultCategory.getId();

        RestAssuredMockMvc.given().
                contentType(ContentType.JSON).
                when().
                delete("/defaultCategories/" + defCatId).
                then().
                statusCode(HttpURLConnection.HTTP_NO_CONTENT); //204

         defaultCategory = catRepo.findOne(defCatId);

        assertNull("category not deleted",defaultCategory);

        RestAssuredMockMvc.given()
                .contentType(ContentType.JSON)
                .when()
                .get("/defaultCategories/" + defCatId)
                .then()
                .assertThat()
                .statusCode(HttpURLConnection.HTTP_NOT_FOUND); //404
    }
}