package szum.expenses.restcontrollers;

import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import szum.expenses.entity.User;
import szum.expenses.entity.generators.UserBuilder;
import szum.expenses.restrepository.UserRepository;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
@WebAppConfiguration
public class UserRepositoryCustomTest {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected UserRepository userRepo;

    @Autowired
    protected UserBuilder userBuilder;

    @Before
    public void configureMockMvcInstance() {
        RestAssuredMockMvc.webAppContextSetup(wac);
    }

    @Before
    @After
    public void cleanDatabase() {
        userRepo.deleteAll();
    }

    @Test
    public void save_test() {

        User user = userRepo.save(userBuilder.withLogin("login").withPassword("pass").build());

        assertEquals("password has not been decoded correctly", "d74ff0ee8da3b9806b18c877dbf29bbde50b5bd8e4dad7a3a725000feb82e8f1", user.getPassword());

        Map<String, String> body = new HashMap<>();
        body.put("password", "haslo");
        body.put("login", "login");

            RestAssuredMockMvc.given()
                .contentType(ContentType.JSON)
                .body(body)
                .when()
                .post("/users")
                .then()
                .statusCode(HttpURLConnection.HTTP_CREATED);
    }

    @Test
    public void update_password_test() {

        User user = userRepo.save(userBuilder.withLogin("login").withPassword("pass").build());

        assertEquals("password has not been decoded correctly", "d74ff0ee8da3b9806b18c877dbf29bbde50b5bd8e4dad7a3a725000feb82e8f1", user.getPassword());

        Map<String, String> body = new HashMap<>();
        body.put("password", "nowe_haslo");
        RestAssuredMockMvc.given()
                .contentType(ContentType.JSON)
                .body(body)
                .when()
                .patch("/users/" + user.getId())
                .then()
                .statusCode(HttpURLConnection.HTTP_NO_CONTENT);

        user = userRepo.findOne(user.getId());
        assertEquals("password has not been updated correctly", "b21647c95aaca60f65ff0410f5083b86b423b04f12e13d8b710641bcc28bfc86", user.getPassword());
    }
}