package szum.expenses.mobile.utils;

/**
 * Created by blazej on 14.04.17.
 */

public interface Consumer<T> {

    void accept(T toBeConsumed);
}
