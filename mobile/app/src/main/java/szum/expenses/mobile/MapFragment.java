package szum.expenses.mobile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.List;

import szum.expenses.mobile.dto.Location;
import szum.expenses.mobile.utils.Consumer;


public class MapFragment extends Fragment implements OnMapReadyCallback {

    private Handler mHandler;

    private IconGenerator iconFactory;
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    private static final String TAG = MapFragment.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        return inflater.inflate(R.layout.map_fragment, container, false);
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();

        mHandler = new Handler(getActivity().getMainLooper());
        iconFactory = new IconGenerator(getContext());

        mMapView = (MapView) view.findViewById(R.id.mapview);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.218029, 20.981992), 14));
            }
        });

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {

                ((ExpensesActivity) getActivity()).onServiceReady(new Consumer<ExpensesService>() {
                    @Override
                    public void accept(final ExpensesService service) {
                        final String id = marker.getSnippet();
                        service.getPlacesWebsite(id, new Consumer<String>() {
                            @Override
                            public void accept(final String url) {
                                if (url != null) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getContext(), "otwieram: " + url, Toast.LENGTH_SHORT);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                return true;
            }
        });

        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener()

        {
            @Override
            public void onCameraChange(final CameraPosition cp) {
                ((ExpensesActivity) getActivity()).onServiceReady(new Consumer<ExpensesService>() {
                    @Override
                    public void accept(final ExpensesService service) {
                        service.getMostPopularPlaces(new Consumer<String>() {
                            @Override
                            public void accept(String places) {
                                service.getPlaces(cp.target.latitude, cp.target.longitude, 1000, places, new LocationConsumer());
                                Toast.makeText(getContext(), "sugerowane miejsca na podstawie: " + places, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });
    }

    public void refresh() {
//        ((ExpensesActivity) getActivity()).onServiceReady(new Consumer<ExpensesService>() {
//            @Override
//            public void accept(final ExpensesService service) {
//                service.getMostPopularPlaces(new Consumer<String>() {
//                    @Override
//                    public void accept(String places) {
//                        service.getPlaces(52.218029, 20.981992, 1000, places, new LocationConsumer());
//                        Toast.makeText(getContext(), "sugerowane miejsca na podstawie: " + places, Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        });
    }

    class LocationConsumer implements Consumer<List<Location>> {

        public LocationConsumer() {
        }

        @Override
        public void accept(final List<Location> locations) {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mGoogleMap.clear();
                    for (final Location l : locations) {
                        iconFactory.setStyle(IconGenerator.STYLE_BLUE);
                        MarkerOptions markerOptions = new MarkerOptions().
                                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(l.name))).
                                position(l.latlng).
                                snippet(l.placeid).
                                anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
                        mGoogleMap.addMarker(markerOptions);
                    }
                }
            });
        }
    }
}