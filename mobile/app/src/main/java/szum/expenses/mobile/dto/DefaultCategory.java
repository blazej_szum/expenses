package szum.expenses.mobile.dto;

import java.util.Calendar;


public class DefaultCategory {

    private Long id;
    private String category;
    private Calendar additionDate;
    private Calendar lastEditionDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Calendar getAdditionDate() {
        return additionDate;
    }

    public void setAdditionDate(Calendar additionDate) {
        this.additionDate = additionDate;
    }

    public Calendar getLastEditionDate() {
        return lastEditionDate;
    }

    public void setLastEditionDate(Calendar lastEditionDate) {
        this.lastEditionDate = lastEditionDate;
    }
}
