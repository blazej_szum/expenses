package szum.expenses.mobile.dto;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by blazej on 29.04.17.
 */

public class EbayItem {

    public String itemId;
    public String title;
    public String subtitle;
    public String categoryId;
    public String categoryName;
    public String galleryURL;
    public String viewItemURL;
    public String currencyId;
    public String money;

    public EbayItem() {
    }

    public EbayItem(String itemId, String title, String subtitle, String categoryId, String categoryName, String galleryURL, String viewItemURL, String currencyId, String money) {
        this.itemId = itemId;
        this.title = title;
        this.subtitle = subtitle;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.galleryURL = galleryURL;
        this.viewItemURL = viewItemURL;
        this.currencyId = currencyId;
        this.money = money;
    }

    @Override
    public String toString() {
        return "EbayItem{" +
                "itemId='" + itemId + "\'\n" +
                ", title='" + title + "\'\n" +
                ", subtitle='" + subtitle + "\'\n" +
                ", categoryId='" + categoryId + "\'\n" +
                ", categoryName='" + categoryName + "\'\n" +
                ", galleryURL='" + galleryURL + "\'\n" +
                ", viewItemURL='" + viewItemURL + "\'\n" +
                ", currencyId='" + currencyId + "\'\n" +
                ", __value__='" + money + "\'\n" +
                '}';
    }
}