package szum.expenses.mobile.dto;

import com.google.android.gms.maps.model.LatLng;

import java.util.Arrays;

/**
 * Created by blazej on 29.04.17.
 */

public class Location {

    public LatLng latlng;
    public String icon;
    public String name;
    public String[] types;
    public String vicinity;
    public String placeid;

    @Override
    public String toString() {
        return "Location{" +
                ",\n latlng=" + latlng +
                ",\n icon='" + icon + '\'' +
                ",\n name='" + name + '\'' +
                ",\n placeid='" + placeid + '\'' +
                ",\n types=" + Arrays.toString(types) +
                ",\n vicinity='" + vicinity + '\'' +
                '}';
    }
}