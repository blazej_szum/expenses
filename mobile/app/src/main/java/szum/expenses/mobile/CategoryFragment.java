package szum.expenses.mobile;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import szum.expenses.mobile.dto.Category;
import szum.expenses.mobile.utils.Consumer;

public class CategoryFragment extends Fragment {

    ListAdapter listAdapter;
    private View mProgressView;

    private static final String TAG = CategoryFragment.class.getSimpleName();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.categories_fragment, container, false);
        mProgressView = view.findViewById(R.id.category_progress);
        return view;
    }

    public void refresh() {
        showProgress(true);
        ((ExpensesActivity) getActivity()).onServiceReady(new Consumer<ExpensesService>() {

            @Override
            public void accept(final ExpensesService expensesService) {
                expensesService.getCategories(new Consumer<List<Category>>() {
                    @Override
                    public void accept(final List<Category> categories) {
                        if (CategoryFragment.this.getActivity() != null) {
                            CategoryFragment.this.getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    if (getContext() != null) {
                                        listAdapter = new ListAdapter(categories, getContext(), expensesService);
                                        adapterView.setAdapter(listAdapter);
                                        showProgress(false);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    ExpandableListView adapterView;

    @Override
    public void onResume() {
        super.onResume();

        View view = getView();
        adapterView = (ExpandableListView) view.findViewById(R.id.categories_list);
        refresh();
    }

    class ListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater mInflater;
        private ExpensesService expensesService;
        private Map<String, List<Category>> groups = new LinkedHashMap<>();
        private List<String> groupList = new ArrayList<>();
        Integer gp = null, cp = null;
        Boolean canAdd = true;
        private List<Category> categories;

        public ListAdapter(List<Category> categories, Context context, ExpensesService expensesService) {
            this.mInflater = LayoutInflater.from(context);
            this.expensesService = expensesService;
            this.categories = categories;

            groups.put(null, new ArrayList<Category>());
            for (Category category : categories) {
                if (category.getActive()) {
                    String group = category.getCategoryGroup();
                    List<Category> list = groups.get(group);
                    if (list == null) {
                        list = new ArrayList<>();
                        groups.put(group, list);
                        groupList.add(group);
                    }
                    list.add(category);
                }
            }
        }

        @Override
        public int getGroupCount() {
            return groupList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return groups.get(groupList.get(groupPosition)).size();
        }

        @Override
        public List<Category> getGroup(int groupPosition) {
            return groups.get(groupPosition);
        }

        @Override
        public Category getChild(int groupPosition, int childPosition) {
            return groups.get(groupList.get(groupPosition)).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int i, boolean isExpanded, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.categories_element, null);
            }

            final TextView textView = (TextView) convertView.findViewById(R.id.category_text);
            final ImageButton add_cat_btn = (ImageButton) convertView.findViewById(R.id.add_cat_btn);
            textView.setFocusable(false);
            add_cat_btn.setFocusable(false);

            if (isExpanded) {
                add_cat_btn.setVisibility(View.VISIBLE);
            } else {
                add_cat_btn.setVisibility(View.INVISIBLE);
            }

            final String group = groupList.get(i);
            final List<Category> categoriesInGroup = groups.get(group);
            if (group != null) {
                textView.setText(group.toUpperCase());
            } else {
                textView.setText("CUSTOM");
            }

            add_cat_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (gp == null && cp == null && canAdd) {
                        canAdd = false;
                        Category category = new Category("give me the name plz :)");
                        category.setCategoryGroup(group);
                        categoriesInGroup.add(category);
                        notifyDataSetChanged();
                    }
                }
            });

            return convertView;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            final Category category = getChild(groupPosition, childPosition);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.categories_edit_element, null);
            }

            final TextInputLayout textInputLayout = (TextInputLayout) convertView.findViewById(R.id.category_editable_text_layout);

            final TextInputEditText editText = (TextInputEditText) convertView.findViewById(R.id.category_editable_text);
            final TextView text = (TextView) convertView.findViewById(R.id.category_text_2);
            final ImageButton editBtn = (ImageButton) convertView.findViewById(R.id.edit_cat_btn);
            final ImageButton saveBtn = (ImageButton) convertView.findViewById(R.id.save_cat_btn);
            ImageButton delBtn = (ImageButton) convertView.findViewById(R.id.del_cat_btn);

            if (category.getDefaultCategory() != null && category.getDefaultCategory()) {
                editBtn.setVisibility(View.INVISIBLE);
                delBtn.setVisibility(View.INVISIBLE);
            } else {
                editBtn.setVisibility(View.VISIBLE);
                delBtn.setVisibility(View.VISIBLE);
            }

            text.setVisibility(View.VISIBLE);
            saveBtn.setVisibility(View.INVISIBLE);
            textInputLayout.setVisibility(View.INVISIBLE);

            text.setText(category.getCategory());
            if (category.getCategory().equals("give me the name plz :)")) {
                gp = groupPosition;
                cp = childPosition;
                canAdd = false;
                text.setVisibility(View.INVISIBLE);
                textInputLayout.setVisibility(View.VISIBLE);
                editBtn.setVisibility(View.INVISIBLE);
                saveBtn.setVisibility(View.VISIBLE);
                editText.requestFocus();
            }

            if (category.getCategory() != null) {
                editText.setText(category.getCategory());
            } else {
                editText.setText("");
            }

            if (gp != null && groupPosition == gp && childPosition == cp) {
                editText.requestFocus();
            }

            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gp = groupPosition;
                    cp = childPosition;
                    canAdd = false;
                    text.setVisibility(View.INVISIBLE);
                    textInputLayout.setVisibility(View.VISIBLE);
                    editBtn.setVisibility(View.INVISIBLE);
                    saveBtn.setVisibility(View.VISIBLE);
                    editText.requestFocus();
                }
            });

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gp = cp = null;
                    canAdd = true;
                    editBtn.setVisibility(View.VISIBLE);
                    saveBtn.setVisibility(View.INVISIBLE);
                    category.setCategory(editText.getText().toString());
                    expensesService.saveCategory(category);
                    notifyDataSetChanged();
                }
            });

            delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (category.getId() != null) {
                        expensesService.deleteCategory(category);
                    } else {
                        canAdd = true;
                    }
                    gp = cp = null;
                    delete(groupPosition, childPosition);
                    notifyDataSetChanged();
                }
            });
            return convertView;
        }

        private void delete(int grupNo, int childNo) {
            groups.get(groupList.get(grupNo)).remove(childNo);
        }


        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }
}
