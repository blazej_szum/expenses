package szum.expenses.mobile.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Category {

    private Long id;
    private Calendar additionDate;
    private Calendar lastEditionDate;
    private String category;
    private String categoryGroup;
    private String locationTags;
    private String buyTags;
    private Long userId;
    private Boolean defaultCategory = false;
    private Boolean active = true;

    public Category(){}

    public Category(String s) {
        this.category = s;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getAdditionDate() {
        return additionDate;
    }

    public void setAdditionDate(Calendar additionDate) {
        this.additionDate = additionDate;
    }

    public Calendar getLastEditionDate() {
        return lastEditionDate;
    }

    public void setLastEditionDate(Calendar lastEditionDate) {
        this.lastEditionDate = lastEditionDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(Set<Expense> expenses) {
        this.expenses = expenses;
    }

    private Set<Expense> expenses = new HashSet<>();

    public Boolean getDefaultCategory() {
        return defaultCategory;
    }

    public void setDefaultCategory(Boolean defaultCategory) {
        this.defaultCategory = defaultCategory;
    }

    public String getCategoryGroup() {
        return categoryGroup;
    }

    public void setCategoryGroup(String categoryGroup) {
        this.categoryGroup = categoryGroup;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getLocationTags() {
        return locationTags;
    }

    public void setLocationTags(String locationTags) {
        this.locationTags = locationTags;
    }

    public String getBuyTags() {
        return buyTags;
    }

    public void setBuyTags(String buyTags) {
        this.buyTags = buyTags;
    }

    @Override
    public String toString() {
        return category;
    }
}
