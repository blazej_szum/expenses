package szum.expenses.mobile;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import szum.expenses.mobile.dto.Category;
import szum.expenses.mobile.dto.Expense;
import szum.expenses.mobile.utils.Consumer;

public class ExpensesFragment extends Fragment {

    private final String TAG = ExpensesFragment.class.getSimpleName();

    ListAdapter listAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.expenses_fragment, container, false);
        return view;
    }

    private void fetchUpToDateCategories() {
        ((ExpensesActivity) getActivity()).onServiceReady(new Consumer<ExpensesService>() {
            @Override
            public void accept(final ExpensesService expensesService) {
                listAdapter = new ListAdapter(getContext(), expensesService);
                expensesService.getExpeneses(new Consumer<List<Expense>>() {
                    @Override
                    public void accept(final List<Expense> expenses) {
                        ExpensesFragment.this.getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                listAdapter.setExpenses(expenses);

                                expensesService.getCategories(new Consumer<List<Category>>() {
                                    @Override
                                    public void accept(final List<Category> categories) {
                                        if (ExpensesFragment.this.getActivity() != null) {
                                            ExpensesFragment.this.getActivity().runOnUiThread(new Runnable() {
                                                public void run() {
                                                    listAdapter.setCategories(categories);
                                                    adapterView.setAdapter(listAdapter);
                                                }
                                            });
                                        }
                                    }
                                });

                            }
                        });
                    }
                });
            }
        });
    }

    int nowy = -1;
    int previousGroup = -1;
    ExpandableListView adapterView;

    float dX;
    float dY;
    int move = 0;

    int choosenGroupPosition = -1;
    int choosenCategoryPosition = -1;


    @Override
    public void onResume() {
        super.onResume();
        nowy = -1;

        View view = getView();
        adapterView = (ExpandableListView) view.findViewById(R.id.expenses_list);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_expenses);
        fab.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        dX = view.getX() - event.getRawX();
                        dY = view.getY() - event.getRawY();
                        move = 0;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        view.setY(event.getRawY() + dY);
                        view.setX(event.getRawX() + dX);
                        move++;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (move <= 3) {
                            if (nowy <= 0) {
                            Toast.makeText(getContext(), "you can fill the form now", Toast.LENGTH_SHORT).show();
                                nowy = listAdapter.addExpense();
                                if (previousGroup != -1) {
                                    adapterView.collapseGroup(previousGroup);
                                }
                                previousGroup = nowy;
                                adapterView.expandGroup(nowy);
                                listAdapter.notifyDataSetChanged();
                            }
                        }
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        adapterView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            // Keep track of previous expanded parent

            @Override
            public void onGroupExpand(int groupPosition) {
                // Collapse previous parent if expanded.
                if (previousGroup > -1 && groupPosition != previousGroup) {
                    adapterView.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });

        fetchUpToDateCategories();
    }

    class ListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater mInflater;
        private List<Expense> expenses;
        private Map<Long, Category> categoriesMap = new HashMap<>();
        private List<Category> activeCategories;
        private Map<String, List<Category>> categoriesByGroup;
        private ExpensesService expensesService;

        public ListAdapter(Context context, ExpensesService expensesService) {
            this.mInflater = LayoutInflater.from(context);
            this.expensesService = expensesService;
        }

        public void setExpenses(List<Expense> expenses) {
            this.expenses = expenses;
        }

        public void setCategories(List<Category> categories) {
            for (Category cat : categories) {
                categoriesMap.put(cat.getId(), cat);
            }

            this.activeCategories = new ArrayList<>();
            this.categoriesByGroup = new HashMap<>();
            for (Category category : categories) {
                if (category.getActive()) {
                    this.activeCategories.add(category);
                }
                List<Category> groupCategories;
                groupCategories = categoriesByGroup.get(category.getCategoryGroup());
                if (groupCategories == null) {
                    groupCategories = new ArrayList<>();
                    categoriesByGroup.put(category.getCategoryGroup(), groupCategories);
                }
                groupCategories.add(category);
            }
        }

        private String translateCategory(Expense expense) {
            Category cat = categoriesMap.get(expense.getCategoryId());
            if (cat != null) {
                return cat.getCategory();
            } else {
                return "NULL CATEGORY!!";
            }
        }

        public int addExpense() {
            this.expenses.add(new Expense());
            return expenses.size() - 1;
        }

        @Override
        public int getGroupCount() {
            return expenses.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return null;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
            notifyDataSetChanged();
        }

        @Override
        public View getGroupView(final int position, boolean isExpanded, View convertView, ViewGroup parent) {
            final Expense expense = expenses.get(position);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.expenses_element, null);
            }

            ImageButton delBtn = (ImageButton) convertView.findViewById(R.id.remove_exp_btn);
            delBtn.setFocusable(false);

            if (isExpanded) {
                delBtn.setVisibility(View.VISIBLE);
            } else {
                delBtn.setVisibility(View.INVISIBLE);
            }

            final TextView expenseText = (TextView) convertView.findViewById(R.id.expense_text);
            final TextView commentText = (TextView) convertView.findViewById(R.id.expense_comment);

            if (expense.getCategoryId() != null || expense.getMoney() != null) {
                expenseText.setText(String.format("%10.2f zł:  %s", expense.getMoney(), translateCategory(expense)).toUpperCase());
            } else {
                expenseText.setText("nowy wydatek".toUpperCase());
            }

            if (expense.getComment() == null) {
                commentText.setText("");
            } else {
                commentText.setText(expense.getComment());
            }

            delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterView.collapseGroup(previousGroup);
                    if (expense.getId() != null) {
                        expensesService.deleteExpense(expense);
                    } else {
                        nowy = -1;
                        previousGroup = -1;
                    }
                    expenses.remove(expense);
                    ListAdapter.this.notifyDataSetChanged();
                    choosenGroupPosition = -1;
                    choosenCategoryPosition = -1;
                }
            });

            return convertView;
        }

        Boolean comment = false;

        @Override
        public View getChildView(int position, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            final Expense expense = expenses.get(position);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.expenses_edit_element, null);
            }

            final TextInputEditText moneyEdit = (TextInputEditText) convertView.findViewById(R.id.expense_editable_text);
            final Spinner categorySpinner = (Spinner) convertView.findViewById(R.id.category_spinner);
            final Spinner categoryGroupSinner = (Spinner) convertView.findViewById(R.id.category_group_spinner);
            final TextInputEditText commentEdit = (TextInputEditText) convertView.findViewById(R.id.expense_comment_edit);
            if (comment) {
                commentEdit.requestFocus();
            } else {
                moneyEdit.requestFocus();
            }

            ImageButton saveBtn = (ImageButton) convertView.findViewById(R.id.save_exp_btn);


            final String[] groups = categoriesByGroup.keySet().toArray(new String[categoriesByGroup.keySet().size()]);
            ArrayAdapter<String> catGroupAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, groups);
            categoryGroupSinner.setAdapter(catGroupAdapter);
            categoryGroupSinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    choosenGroupPosition = position;
                    List<Category> cat = categoriesByGroup.get(groups[position]);
                    Category[] categoriesOfGroup = cat.toArray(new Category[cat.size()]);
                    ArrayAdapter<Category> catAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categoriesOfGroup);
                    categorySpinner.setAdapter(catAdapter);
                    if (choosenCategoryPosition > -1) {
                        categorySpinner.setSelection(choosenCategoryPosition);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {}
            });

            if (choosenGroupPosition > -1) {
                categoryGroupSinner.setSelection(choosenGroupPosition);
            }

            if (choosenCategoryPosition > -1) {
                List<Category> cat = categoriesByGroup.get(groups[choosenGroupPosition]);
                Category[] categoriesOfGroup = cat.toArray(new Category[cat.size()]);
                ArrayAdapter<Category> catAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categoriesOfGroup);
                categorySpinner.setAdapter(catAdapter);
                categorySpinner.setSelection(choosenCategoryPosition);
            }

            categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    choosenCategoryPosition = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {}
            });

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (moneyEdit.getText().toString() == "" || categorySpinner.getSelectedItem() == null) {
                        Toast.makeText(getContext(), "fill up the form first", Toast.LENGTH_SHORT).show();
                    }
                    expense.setMoney(new BigDecimal(moneyEdit.getText().toString()));
                    expense.setComment(commentEdit.getText().toString());
                    expense.setCategoryId(((Category) categorySpinner.getSelectedItem()).getId());
                    expensesService.saveExpense(expense);
                    adapterView.collapseGroup(previousGroup);
                    nowy = -1;
                    notifyDataSetChanged();
                    choosenGroupPosition = -1;
                    choosenCategoryPosition = -1;
                }
            });

            commentEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        comment = true;
                    }
                }
            });
            moneyEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        comment = false;
                    }
                }
            });

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }

    }
}
