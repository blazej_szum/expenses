package szum.expenses.mobile;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import szum.expenses.mobile.dto.EbayItem;
import szum.expenses.mobile.utils.Consumer;

public class ChoosenForYouFragment extends Fragment {

    private static final String TAG = ChoosenForYouFragment.class.getSimpleName();

    ListAdapter listAdapter;
    ListView adapterView;
    View mProgressView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.choosen_for_you_fragment, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        adapterView = (ListView) view.findViewById(R.id.choosen_for_you_list);
        listAdapter = new ListAdapter(getContext());
        mProgressView = view.findViewById(R.id.chosen_progress);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_chosen);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });
    }

    public void refresh() {
        showProgress(true);
        listAdapter.setItems(new ArrayList<EbayItem>());
        listAdapter.notifyDataSetChanged();

        ((ExpensesActivity) getActivity()).onServiceReady(new Consumer<ExpensesService>() {

            @Override
            public void accept(final ExpensesService expensesService) {

                expensesService.getKeywords(new Consumer<String>() {
                    @Override
                    public void accept(final String keywords) {

                        if (keywords == null) {
                            showProgress(false);
                            return;
                        }

                        expensesService.getEbayItems(keywords, new Consumer<List<EbayItem>>() {
                            @Override
                            public void accept(final List<EbayItem> items) {

                                ChoosenForYouFragment.this.getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getContext(), "sugerowane produkty na podstawie: " + keywords, Toast.LENGTH_SHORT).show();
                                        listAdapter.setItems(items);
                                        adapterView.setAdapter(listAdapter);
                                        showProgress(false);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    class ListAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        private List<EbayItem> items;

        public ListAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public void setItems(List<EbayItem> items) {
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public EbayItem getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final EbayItem item = getItem(position);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.choosen_for_you_element, null);
            }
            final TextView title_for_you = (TextView) convertView.findViewById(R.id.title_for_you);

            if (item.title != null) {
                title_for_you.setText(item.title);
            }

            final TextView sumtitle_for_you = (TextView) convertView.findViewById(R.id.sumtitle_for_you);
            if (item.subtitle != null) {
                sumtitle_for_you.setText(item.subtitle);
            }

            final TextView category_for_you = (TextView) convertView.findViewById(R.id.category_for_you);
            if (item.categoryName != null) {
                category_for_you.setText(item.categoryName);
            }

            final TextView price_for_you = (TextView) convertView.findViewById(R.id.price_for_you);
            if (item.money != null && item.currencyId != null) {
                price_for_you.setText(item.money + " " + item.currencyId);
            }

            ImageButton image = (ImageButton) convertView.findViewById(R.id.image_for_you);
            if (item.galleryURL != null) {
                Picasso.
                        with(getContext()).
                        load(item.galleryURL).resize(300, 300).
                        into(image);
                image.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
            }
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.viewItemURL != null) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(item.viewItemURL));
                        startActivity(i);
                    }
                }
            });

            return convertView;
        }
    }
}