package szum.expenses.mobile;


import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import szum.expenses.mobile.dto.AccessToken;
import szum.expenses.mobile.dto.Category;
import szum.expenses.mobile.dto.EbayItem;
import szum.expenses.mobile.dto.Expense;
import szum.expenses.mobile.dto.Location;
import szum.expenses.mobile.utils.Consumer;

public class ExpensesService extends Service {

    private static final String TAG = ExpensesService.class.getSimpleName();

    private static final String PROTOCOL = "http://";
    private static final String SERV_ADDR = "195.181.212.69";
    private static final String SERV_PORT = ":8080";
    private static final String APP_NAME = "expenses";
    private static final String APP_ADDR = PROTOCOL + SERV_ADDR + SERV_PORT + "/" + APP_NAME;

    private AccessToken accesToken;
    private List<Expense> expensesCache;
    private List<Category> categoryCache;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private final IBinder mBinder = new LocalBinder();

    public String getToken() {
        return accesToken.getToken();
    }

    public void getPlacesWebsite(final String placeId, final Consumer<String> callback) {
        new Thread() {
            @Override
            public void run() {
                String url = "https://maps.googleapis.com/maps/api/place/details/json?" +
                        "placeid=" + placeId +
                        "&key=AIzaSyA57hrHUOMk9rn-c1_8bgRileh_BhUGKIQ";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                try {
                    ResponseEntity<JsonNode> response = restTemplate.getForEntity(url, JsonNode.class);
                    if (response.getStatusCode() == HttpStatus.OK) {

                        JsonNode jsonResponse = response.getBody();
                        JsonNode res = jsonResponse.get("result");
                        String website = res.get("website").asText();
                        callback.accept(website);
                    } else {
                        callback.accept(null);
                    }
                } catch (HttpClientErrorException e) {
                    Log.e("ExpensesService", "WTF?", e);
                    callback.accept(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void getPlaces(final double lat, final double lng, final int radius, final String places, final Consumer<List<Location>> callback) {
        new Thread() {
            @Override
            public void run() {

                Log.d(TAG, "getting places: " + places);

                String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                        "location=" + lat + "," + lng +
                        "&radius=" + radius +
                        "&types=" + places +
                        "&key=AIzaSyA57hrHUOMk9rn-c1_8bgRileh_BhUGKIQ";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                try {
                    ResponseEntity<JsonNode> response = restTemplate.getForEntity(url, JsonNode.class);
                    if (response.getStatusCode() == HttpStatus.OK) {
                        JsonNode jsonResponse = response.getBody();
                        JsonNode results = jsonResponse.get("results");
                        List<Location> locations = new ArrayList<>();
                        for (JsonNode jsonNode : results) {
                            Location location = readLocation(jsonNode);
                            if (location != null) {
                                locations.add(location);
                            }
                        }
                        callback.accept(locations);
                    } else {
                        Log.e("ExpensesService", "response.getStatusCode(): " + response.getStatusCode());
                        callback.accept(null);
                    }
                } catch (HttpClientErrorException e) {
                    Log.e("ExpensesService", "WTF?", e);
                    callback.accept(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private Location readLocation(JsonNode json) {
        Location location = new Location();
        try {
            JsonNode geometry = json.get("geometry");
            JsonNode loc = geometry.get("location");
            location.latlng = new LatLng(loc.get("lat").doubleValue(), loc.get("lng").doubleValue());
            location.icon = json.get("icon").asText();
            location.name = json.get("name").asText();
            location.vicinity = json.get("vicinity").asText();
            location.placeid = json.get("place_id").asText();

            JsonNode types = json.get("types");
            location.types = new String[types.size()];
            for (int i = 0; i < types.size(); i++) {
                location.types[i] = types.get(i).asText();
            }
            return location;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public void getCategories(final Consumer<List<Category>> callback) {
        new Thread() {
            @Override
            public void run() {
                HttpHeaders headers = new HttpHeaders();
                headers.add("X-Access-Token", accesToken.getToken());

                String url = APP_ADDR + "/api/categories/search/findByUserIdOrderByCategoryGroup?" +
                        "userId=" + accesToken.getUser().getId() +
                        "&page=0&size=100";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                try {
                    ResponseEntity<ObjectNode> response = restTemplate.getForEntity(url, ObjectNode.class, headers);
                    if (response.getStatusCode() == HttpStatus.OK) {
                        ObjectNode jsonResponse = response.getBody();
                        JsonNode embedded = jsonResponse.get("_embedded");
                        JsonNode jsonList = embedded.withArray("categories");

                        ObjectMapper mapper = new ObjectMapper();
                        List<Category> catList = mapper.readValue(String.valueOf(jsonList), mapper.getTypeFactory().constructCollectionType(List.class, Category.class));


                        Iterator<JsonNode> jsonCatIter = jsonList.iterator();
                        Iterator<Category> listCatIter = catList.iterator();

                        JsonNode jsonCat;
                        Category cat;
                        while (jsonCatIter.hasNext() && listCatIter.hasNext()) {
                            jsonCat = jsonCatIter.next();
                            cat = listCatIter.next();

                            String href[] = jsonCat.get("_links").get("self").get("href").asText().split("/");
                            Long id = Long.parseLong(href[href.length - 1]);
                            cat.setId(id);
                        }

                        categoryCache = catList;
                        callback.accept(catList);
                    } else {
                        Log.e("ExpensesService", "response.getStatusCode(): " + response.getStatusCode());
                        callback.accept(null);
                    }
                } catch (HttpClientErrorException e) {
                    Log.e("ExpensesService", "WTF?", e);
                    callback.accept(null);
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void saveCategory(final Category category) {

        categoryCache.add(category);

        category.setUserId(accesToken.getUser().getId());

        new Thread() {
            @Override
            public void run() {
                HttpHeaders headers = new HttpHeaders();
                headers.add("X-Access-Token", accesToken.getToken());

                String url = APP_ADDR + "/api/categories";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                try {
                    ResponseEntity<Category> response;
                    if (category.getId() == null) {
                        response = restTemplate.postForEntity(url, category, Category.class, headers);
                        if (Arrays.asList(HttpStatus.OK, HttpStatus.CREATED).contains(response.getStatusCode())) {
                        } else {
                            Log.e("ExpensesService", "error while saving category: " + response.getStatusCode());
                        }
                    } else {
                        url += "/" + category.getId();
                        restTemplate.put(url, category, headers);
                    }
                } catch (Exception e) {
                    Log.e("ExpensesService", "WTF?", e);
                }
            }
        }.start();
    }

    public void deleteCategory(final Category category) {

        category.setActive(false);

        new Thread() {
            @Override
            public void run() {
                HttpHeaders headers = new HttpHeaders();
                headers.add("X-Access-Token", accesToken.getToken());

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                if (category.getId() != null) {
                    String url = APP_ADDR + "/api/categories/" + category.getId();
                    restTemplate.put(url, category, headers);
                }
            }
        }.start();
    }

    public void deleteExpense(final Expense expense) {
        new Thread() {
            @Override
            public void run() {
                HttpHeaders headers = new HttpHeaders();
                headers.add("X-Access-Token", accesToken.getToken());

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                if (expense.getId() != null) {
                    String url = APP_ADDR + "/api/expenses/" + expense.getId();
                    restTemplate.delete(url, headers);
                }
            }
        }.start();
    }

    public void getKeywords(final Consumer<String> callback) {

        getCachedExpences(new Consumer<List<Expense>>() {
            @Override
            public void accept(List<Expense> expenses) {
                if (expenses.isEmpty()) {
                    callback.accept(null);
                    return;
                }

                final Expense lastExpense = expenses.get(expenses.size() - 1);
                if (lastExpense.getComment() != "" && lastExpense.getComment() != null) {
                    callback.accept(filter(lastExpense.getComment()));
                } else {
                    getCachedCategories(new Consumer<List<Category>>() {
                        @Override
                        public void accept(List<Category> categories) {
                            Category cat = null;
                            for (Category catt : categories) {
                                if (catt.getId() == lastExpense.getCategoryId()) {
                                    cat = catt;
                                }
                            }
                            if (cat != null) {
                                callback.accept(filter(cat.getCategory()));
                            }
                        }
                    });
                }
            }
        });


    }

    private String filter(String zeSpacjami) {
        return "(" + zeSpacjami.replace(" ", ",") + ")";
    }

    public void getExpeneses(final Consumer<List<Expense>> callback) {
        new Thread() {
            @Override
            public void run() {

                HttpHeaders headers = new HttpHeaders();
                headers.add("X-Access-Token", accesToken.getToken());

                String url = APP_ADDR + "/api/expenses/search/findByUserId?userId=" + accesToken.getUser().getId();
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                try {
                    ResponseEntity<ObjectNode> response = restTemplate.getForEntity(url, ObjectNode.class, headers);
                    if (response.getStatusCode() == HttpStatus.OK) {
                        ObjectNode jsonResponse = response.getBody();
                        JsonNode embedded = jsonResponse.get("_embedded");
                        JsonNode jsonList = embedded.withArray("expenses");

                        ObjectMapper mapper = new ObjectMapper();
                        List<Expense> list = mapper.readValue(String.valueOf(jsonList), mapper.getTypeFactory().constructCollectionType(List.class, Expense.class));

                        Iterator<JsonNode> jsonIter = jsonList.iterator();
                        Iterator<Expense> listIter = list.iterator();

                        JsonNode json;
                        Expense expense;
                        while (jsonIter.hasNext() && listIter.hasNext()) {
                            json = jsonIter.next();
                            expense = listIter.next();

                            String href[] = json.get("_links").get("self").get("href").asText().split("/");
                            Long id = Long.parseLong(href[href.length - 1]);
                            expense.setId(id);
                        }

                        expensesCache = list;
                        callback.accept(list);
                    } else {
                        Log.e("ExpensesService", "response.getStatusCode(): " + response.getStatusCode());
                        callback.accept(null);
                    }
                } catch (Exception e) {
                    Log.e("ExpensesService", "WTF?", e);
                    callback.accept(null);
                }
            }
        }.start();
    }

    public void getEbayItems(final String keyword, final Consumer<List<EbayItem>> consumer) {
        new Thread() {
            @Override
            public void run() {

                Log.d("Service", "getting ebay items...");

                String url = "http://svcs.ebay.com/services/search/FindingService/v1?" +
                        "OPERATION-NAME=findItemsAdvanced" +
                        "&SERVICE-VERSION=1.0.0" +
                        "&SECURITY-APPNAME=BlazejSz-testapp-PRD-369dbd521-a1ad31ea" +
                        "&RESPONSE-DATA-FORMAT=JSON" +
                        "&REST-PAYLOAD=true" +
                        "&paginationInput.entriesPerPage=10" +
                        "&keywords=" + keyword;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                try {
                    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
                    if (response.getStatusCode() == HttpStatus.OK) {

                        JSONObject json = new JSONObject(response.getBody());

                        JSONObject _1 = (JSONObject) json.getJSONArray("findItemsAdvancedResponse").get(0);
                        JSONObject _2 = (JSONObject) _1.getJSONArray("searchResult").get(0);
                        JSONArray items = _2.getJSONArray("item");

                        List<EbayItem> ebayItems = new ArrayList<>();
                        for (int i = 0; i < items.length(); i++) {
                            EbayItem item = convertToEbayItem((JSONObject) items.get(i));
                            if (item != null) {
                                ebayItems.add(item);
                            }
                        }
                        consumer.accept(ebayItems);
                    } else {
                        Log.e("ExpensesService", "response.getStatusCode(): " + response.getStatusCode());
                    }
                } catch (Exception e) {
                    Log.e("ExpensesService", "WTF?", e);
                }
            }
        }.start();
    }

    private void getCachedExpences(final Consumer<List<Expense>> consumer) {
        if (expensesCache == null) {
            getExpeneses(new Consumer<List<Expense>>() {
                @Override
                public void accept(List<Expense> toBeConsumed) {
                    expensesCache = toBeConsumed;
                    consumer.accept(expensesCache);
                }
            });
        } else {
            consumer.accept(expensesCache);

        }
    }

    private void getCachedCategories(final Consumer<List<Category>> consumer) {
        if (categoryCache == null) {
            getCategories(new Consumer<List<Category>>() {
                @Override
                public void accept(List<Category> toBeConsumed) {
                    categoryCache = toBeConsumed;
                    consumer.accept(categoryCache);
                }
            });
        } else {
            consumer.accept(categoryCache);
        }
    }

    public void saveExpense(final Expense expense) {

        expense.setUserId(accesToken.getUser().getId());

        new Thread() {
            @Override
            public void run() {
                HttpHeaders headers = new HttpHeaders();
                headers.add("X-Access-Token", accesToken.getToken());

                String url = APP_ADDR + "/api/expenses";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                try {
                    ResponseEntity<JsonNode> response;
                    if (expense.getId() == null) {
                        response = restTemplate.postForEntity(url, expense, JsonNode.class, headers);
                        if (Arrays.asList(HttpStatus.OK, HttpStatus.CREATED).contains(response.getStatusCode())) {

                            JsonNode json = response.getBody();
                            String href[] = json.get("_links").get("self").get("href").asText().split("/");
                            Long id = Long.parseLong(href[href.length - 1]);
                            expense.setId(id);

                        } else {
                            Log.e("ExpensesService", "error while saving category: " + response.getStatusCode());
                        }
                    } else {
                        url += "/" + expense.getId();
                        restTemplate.put(url, expense, headers);
                    }
                } catch (Exception e) {
                    Log.e("ExpensesService", "WTF?", e);
                }
            }
        }.start();
    }

    public void getMostPopularPlaces(final Consumer<String> consumer) {

        getCachedCategories(new Consumer<List<Category>>() {
            @Override
            public void accept(List<Category> categories) {
                final Map<Long, List<String>> catMap = new HashMap<>();

                for (Category category : categories) {
                    if (category.getLocationTags() != null) {
                        catMap.put(category.getId(), Arrays.asList(category.getLocationTags().split(",")));
                    }
                }

                getCachedExpences(new Consumer<List<Expense>>() {
                    @Override
                    public void accept(List<Expense> expenses) {
                        Map<String, Counter> mapa = new HashMap<>();
                        for (Expense ex : expenses) {
                            if (ex.getCategoryId() != null && catMap.get(ex.getCategoryId()) != null) {
                                for (String type : catMap.get(ex.getCategoryId())) {
                                    Counter cntr = mapa.get(type);
                                    if (cntr == null) {
                                        cntr = new Counter(type);
                                        mapa.put(type, cntr);
                                    }
                                    cntr.i++;
                                }
                            }
                        }
                        List<Counter> list = new ArrayList<>(mapa.values());
                        Collections.sort(list, new Comparator<Counter>() {
                            @Override
                            public int compare(Counter o1, Counter o2) {
                                return o1.i - o2.i;
                            }
                        });

                        int SIZE = 3;
                        String frequent = "";
                        for (int i = 0; i < SIZE && i < list.size(); i++) {
                            frequent = frequent + "|" + list.get(i).type;
                        }
                        consumer.accept(frequent);
                    }
                });
            }
        });
    }

    class Counter {
        int i = 0;
        String type;

        public Counter(String type) {
            this.type = type;
        }
    }

    public class LocalBinder extends Binder {
        ExpensesService getService() {
            return ExpensesService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
    }

    public void login(final String username, final String password, final Consumer<Boolean> callback) {
        new Thread() {
            @Override
            public void run() {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                map.add("username", username);
                map.add("password", password);

                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

                String url = APP_ADDR + "/auth/authenticate";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                try {
                    ResponseEntity<AccessToken> response = restTemplate.postForEntity(url, request, AccessToken.class);
                    if (response.getStatusCode() == HttpStatus.OK) {
                        accesToken = response.getBody();
                        callback.accept(true);
                    } else {
                        callback.accept(false);
                    }
                } catch (HttpClientErrorException e) {
                    callback.accept(false);
                }
            }
        }.start();
    }

    private EbayItem convertToEbayItem(JSONObject o) {
        EbayItem item = new EbayItem();
        item.itemId = (String) fetch(o, "itemId");
        item.title = (String) fetch(o, "title");
        item.subtitle = (String) fetch(o, "subtitle");
        JSONObject primaryCategory = (JSONObject) fetch(o, "primaryCategory");
        item.categoryId = (String) fetch(primaryCategory, "categoryId");
        item.categoryName = (String) fetch(primaryCategory, "categoryName");
        item.galleryURL = (String) fetch(o, "galleryURL");
        item.viewItemURL = (String) fetch(o, "viewItemURL");
        JSONObject sellingStatus = (JSONObject) fetch(o, "sellingStatus");
        JSONObject currentPrice = (JSONObject) fetch(sellingStatus, "currentPrice");
        try {
            item.currencyId = currentPrice.getString("@currencyId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            item.money = currentPrice.getString("__value__");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    private Object fetch(JSONObject o, String name) {
        try {
            return o.getJSONArray(name).get(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}