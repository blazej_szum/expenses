package szum.expenses.mobile.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Calendar;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Expense {

    private Long id;
    private Calendar additionDate;
    private Calendar lastEditionDate;
    private BigDecimal money;
    private Long userId;
    private Long categoryId;
    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Calendar getAdditionDate() {
        return additionDate;
    }

    public void setAdditionDate(Calendar additionDate) {
        this.additionDate = additionDate;
    }

    public Calendar getLastEditionDate() {
        return lastEditionDate;
    }

    public void setLastEditionDate(Calendar lastEditionDate) {
        this.lastEditionDate = lastEditionDate;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
