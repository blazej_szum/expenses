package szum.expenses.mobile;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import szum.expenses.mobile.utils.Consumer;

public class ExpensesActivity extends AppCompatActivity {

    private static final String TAG = ExpensesActivity.class.getSimpleName();


    private ExpensesService expensesService;
    private boolean mBound = false;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ChoosenForYouFragment choosenForYouFragment;
    private MapFragment mapFragment;
    private ExpensesFragment expFragment;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        setTitle("Categories");
                        break;
                    case 1:
                        setTitle("Expenses");
                        if (expFragment != null) {
                            expFragment.fetchUpToDateCategories();
                        }

                        break;
                    case 2:
                        setTitle("Choosen For You");
                        if (choosenForYouFragment != null) {
                            choosenForYouFragment.refresh();
                        }
                        break;
                    case 3:
                        setTitle("Interesting Places");
                        if (mapFragment != null) {
                            mapFragment.refresh();
                        }
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        Intent intent = new Intent(this, ExpensesService.class);

        startService(intent);
    }

    private List<Consumer<ExpensesService>>
            whoIsWaitingForTheService = Collections.synchronizedList(new ArrayList<Consumer<ExpensesService>>());


    public void onServiceReady(Consumer<ExpensesService> expensesServiceConsumer) {
        if (expensesService == null) {
            whoIsWaitingForTheService.add(expensesServiceConsumer);
        } else {
            expensesServiceConsumer.accept(expensesService);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ExpensesService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            ExpensesService.LocalBinder binder = (ExpensesService.LocalBinder) service;
            expensesService = binder.getService();
            mBound = true;

            synchronized (whoIsWaitingForTheService) {
                for (Consumer<ExpensesService> expensesServiceConsumer : whoIsWaitingForTheService) {
                    expensesServiceConsumer.accept(expensesService);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_expenses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ExpensesService getService() {
        return expensesService;
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Do you want to leave the app?")
                .setTitle("Leaving the app")
                .setCancelable(true)
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ExpensesActivity.this.finishAffinity();
                    }
                })
                .show();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        @Override
        public void startUpdate(ViewGroup container) {
            super.startUpdate(container);
        }

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new CategoryFragment();
                case 1:
                    expFragment = new ExpensesFragment();
                    return expFragment;
                case 2:
                    choosenForYouFragment = new ChoosenForYouFragment();
                    return choosenForYouFragment;
                case 3:
                    mapFragment = new MapFragment();
                    return mapFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Categories";
                case 1:
                    return "Your expenses";
                case 2:
                    return "Choosen For You";
                case 3:
                    return "Interesting Places";
            }
            return null;
        }
    }
}
