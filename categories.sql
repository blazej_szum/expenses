truncate table DefaultCategory;
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(01, 'entertainment','books','book_store','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(02, 'entertainment','something to read', 'book_store','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(03, 'entertainment','casino','casino','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(04, 'entertainment','zoo','zoo','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(04, 'entertainment','cinema','cinema','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(10, 'health care','doctor','doctor,health','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(11, 'health care','hospital','hospital, health','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(12, 'health care','dentist','dentist','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(13, 'health care','pharmacy','pharmacy','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(14, 'health care','physiotherapist','physiotherapist','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(20, 'transport','car rental','car_rental','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(30, 'style', 'jewelry','jewelry_store','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(31, 'style', 'hair care','hair_care','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(32, 'style', 'clothes','clothing_store,shopping_mall','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(33, 'style', 'beauty salon','beauty_salon','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(34, 'style', 'gym','gym','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(35, 'style', 'shoes','shoe_store','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(40, 'going out', 'alcohol', 'liquor_store','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(41, 'going out', 'clubbing','night_club','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(42, 'going out', 'bar', 'bar','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(50, 'food', 'food', 'food,grocery_or_supermarket','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(51, 'food', 'dinner','dinner','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(52, 'food', 'restaurant','restaurant','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(53, 'food', 'polish cuisine','restaurant','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(54, 'food', 'italian cuisine','restaurant','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(55, 'food', 'french cuisine','restaurant','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(56, 'food', 'asian cuisine','restaurant','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(60, 'electronics', 'photo cameras','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(61, 'electronics', 'computers','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(62, 'electronics', 'mobile phones','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(63, 'electronics', 'household appliances','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(64, 'electronics', 'TV','','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(70, 'games and consoles', 'computer game','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(71, 'games and consoles', 'console','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(72, 'games and consoles', 'accessories','','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(80, 'home', 'furniture','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(81, 'home', 'garden','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(82, 'home', 'illumination','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(83, 'home', 'home furnishing','','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(90, 'music', 'CDs','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(91, 'music', 'song','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(92, 'music', 'concert ticket','','');

insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(100, 'motorisation', 'spare parts','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(101, 'motorisation', 'maintenance','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(102, 'motorisation', 'fuel','','');
insert into DefaultCategory(id,categoryGroup,category,locationTags,buyTags) values(103, 'motorisation', 'insurance','','');

